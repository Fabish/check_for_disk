#! /bin/sh
## some users might be in the disk group
## this script will check and (if wanted) remove the user from that group.

# No Root allowed here!
if [ "$(whoami)" = "root" ]; then {
    printf "Root now allowed here!";
    exit 1;
}
fi
BAD_GROUP="disk"
FAIL="true"

if groups ${USER} | grep -q "${BAD_GROUP}" ; then {
    printf "it's not secure to stay in group \"${BAD_GROUP}\"\n";
    printf "leave that group? [Y/n]";
    read LEAVE_THAT_GROUP;
    case ${LEAVE_THAT_GROUP} in
        (y|Y)
            printf "running command to remove that groups from users setup: \n\t $ pkexec gpasswd -d ${USER} ${BAD_GROUP}\n";
            pkexec gpasswd -d ${USER} ${BAD_GROUP} && FAIL="false";
        ;;
        (*)
        ;;
    esac
}
else {
    printf "Not in group \"${BAD_GROUP}\" 😁\n";
}
fi
if [ ${FAIL} = "true" ]; then {
    printf "Nothing was changed\n";
}
else {
    printf "please restart your computer now!\n";
}
fi
printf "Bye! 👌\n";    